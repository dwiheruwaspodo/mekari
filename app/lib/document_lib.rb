class DocumentLib

  attr_accessor :document, :title, :image, :path, :canvas, :page, :posX, :posY, :result_path, :override_title

  def initialize(document_tempfile, title, image)
    @document = document_tempfile
    @title = title
    @image = image.tempfile
    @result_path = "storage/#{@title}.pdf"
    @override_title = true
  end

  def read_file
    HexaPDF::Document.open(@document.path)
  end

  def set_page(page)
    @page = page
  end

  def set_canvas
    @canvas = @page.canvas(type: :overlay)
  end

  def set_title
    @canvas.fill_color(129, 192, 255).
      font("Helvetica", size: 25).
      text(@title, at: [25, @page.box.height.to_i - 25])
  end

  def set_images
    @canvas.translate(0, 20) do
      @canvas.line_width(0.5)
      @canvas.opacity(fill_alpha: 0.5, stroke_alpha: 0.2) do
        @canvas.image(@image.path, at: [@posX.to_f, @posY.to_f], height: 80)
      end
    end
  end

  def get_result_path
    @result_path
  end

  def process!
    begin
      doc = read_file
      set_page(doc.pages[0])
      set_canvas
      set_title if @override_title
      set_images

      doc.write(@result_path) ? true : false
    rescue
      false
    end
  end

end
