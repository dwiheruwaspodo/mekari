import React from "react";

export default () => (
    <div className="vw-100 vh-100 primary-color d-flex align-items-center justify-content-center">
        <div className="jumbotron jumbotron-fluid bg-transparent">
            <div className="container secondary-color">
                <h1 className="display-4">Document</h1>
                <p className="lead">
                    Hello this is document pages
                </p>
                <hr className="my-4" />
                <button
                    type={"button"}
                    className="btn btn-lg btn-secondary btn-block"
                    role="button"
                    style={{ width: "100%" }}
                >
                    View Documents
                </button>
            </div>
        </div>
    </div>
);