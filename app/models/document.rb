class Document < ApplicationRecord

  before_create :generate_token

  def generate_token
    self.token = SecureRandom::uuid
    self.recalculate unless valid?
  end

  def recalculate
    self.generate_token
  end

end
