class FileValidation
  attr_accessor :file

    def initialize(file)
      if file.kind_of?(String)
        @file = convert_to_file(file)
      else
        @file = file
      end

      raise "Image not readable" unless @file

      @types = %w(image/jpeg image/png application/pdf)
    end

    def valid?
      @file.present? ? (@types.include? self.mime_type) : false
    end

    def tempfile
      @file.tempfile
    end

    def filename
      @file.filename
    end

    def mime_type
      FileMagic.new(FileMagic::MAGIC_MIME).file(self.tempfile.path, true)
    end

    def to_base64
      "data:#{mime_type};base64," + Base64.strict_encode64(File.read(self.tempfile)) if self.valid?
    end

    def convert_to_file(image_base64)
      temp_image = Tempfile.new('validate_image')
      image_base64 = URI.decode(image_base64)
      image_base64 = image_base64.split(',')[1] if image_base64.start_with?('data:')

      begin
        File.open(temp_image.path, 'wb') do |f|
          f.write(Base64.strict_decode64(image_base64))
        end

        {
          :tempfile => temp_image,
          :filename => nil
        }

        temp_image.close!
      rescue
        nil
      end
    end

  end
