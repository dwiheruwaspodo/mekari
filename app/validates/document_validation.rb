class DocumentValidation
  include ActiveModel::Validations

  attr_accessor :doc, :title, :image, :posX, :posY

  validates :doc, presence: true
  validates :image, presence: true
  validates :posY, presence: true, numericality: true
  validates :posX, presence: true, numericality: true
  validates :title, length: {minimum: 3}, format: {with: /\A'?[a-zA-Z]+(([',. -][a-zA-Z ]?)?[a-zA-Z]*)*\z/, message: 'not allowed to use special characters'}, presence: true
  validate :validate_doc
  validate :validate_image

  def validate_doc
    unless doc.nil?
      doc_validation = ::FileValidation.new(doc)
      errors.add(:doc, 'file must be image (png/jpg) or pdf') unless doc_validation.valid?
    else
      errors.add(:doc, 'file must be image (png/jpg) or pdf')
    end
  rescue
    errors.add(:doc, 'file must be image (png/jpg) or pdf')
    end

  def validate_image
    unless image.nil?
      doc_validation = ::FileValidation.new(image)
      errors.add(:image, 'file must be image (png/jpg) or pdf') unless doc_validation.valid?
    else
      errors.add(:image, 'file must be image (png/jpg) or pdf')
    end
  rescue
    errors.add(:image, 'file must be image (png/jpg) or pdf')
  end
end

