class DocumentUpdateValidation
  include ActiveModel::Validations

  attr_accessor :doc, :title, :image, :posX, :posY

  validates :image, presence: true
  validates :posY, presence: true, numericality: true
  validates :posX, presence: true, numericality: true
  validate :image

  def validate_image
    unless image.nil?
      doc_validation = ::FileValidation.new(image)
      errors.add(:image, 'file must be image (png/jpg) or pdf') unless doc_validation.valid?
    else
      errors.add(:image, 'file must be image (png/jpg) or pdf')
    end
  rescue
    errors.add(:image, 'file must be image (png/jpg) or pdf')
  end
end

