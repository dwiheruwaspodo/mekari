module ApplicationHelper

  def response_data(code, data)
    response = Hash.new
    response[:code] = code
    response[:data] = data
    response
  end

  def response_error(code, data)
    response = Hash.new
    response[:code] = code
    response[:error] = data
    response
  end

end
