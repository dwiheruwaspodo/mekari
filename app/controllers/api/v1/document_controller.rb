module Api
  module V1
    class DocumentController < ApiController
      include ApplicationHelper

      def index
        data = ::Document.all
        render json: response_data(200, data), status: 200
      end

      def create
        # validation input
        validation = validation_document(params)

        # validation error
        render json: response_error(422, validation.errors), status: 422 and return unless validation.valid?

        begin
          # process image
          process_pdf = ::DocumentLib.new(params[:doc].tempfile, params[:title], params[:image])
          process_pdf.posX = params[:posX]
          process_pdf.posY = params[:posY]
          process_pdf.process!

          # save DB
          data = ::Document.create({
                                     title: params[:title],
                                     path: process_pdf.get_result_path
                                   })

          # delete tempfile
          params[:doc].tempfile.close!
          params[:image].tempfile.close!

          render json: response_data(200, data), status: 200 and return
        rescue
          render json: response_error(500, "Internal Server Error."), status: 500 and return
        end
      end

      def show
        data = ::Document.where(token: params[:id]).first

        render json: response_error(404, "Document not found."), status: 404 and return unless data.present?

        render json: response_data(200, data), status: 200
      end

      def update
        # validation input
        validation = validation_update_document(params)

        # validation error
        render json: response_error(422, validation.errors), status: 422 and return unless validation.valid?

        data = ::Document.where(token: params[:id]).first
        render json: response_error(404, "Document not found."), status: 404 and return unless data.present?

        # get file
        file = File.open(Rails.root.join(data.path))

        document_overwrite = Tempfile.new(['document_overwrite', '.pdf'])
        document_overwrite.binmode
        document_overwrite.write(file.read)

        begin
          # process image
          process_pdf = ::DocumentLib.new(document_overwrite, data.title, params[:image])
          process_pdf.posX = params[:posX]
          process_pdf.posY = params[:posY]
          process_pdf.override_title = false
          process_pdf.process!

          # save DB
          data.path = process_pdf.get_result_path
          data.save

          # delete tempfile
          document_overwrite.close!
          params[:image].tempfile.close!

          render json: response_data(200, data), status: 200 and return
        rescue
          render json: response_error(500, "Internal Server Error."), status: 500 and return
        end
      end

      private

      def validation_document(params)
        validation = ::DocumentValidation.new
        validation.doc = params[:doc]
        validation.title = params[:title]
        validation.image = params[:image]
        validation.posX = params[:posX]
        validation.posY = params[:posY]
        validation
      end

      def validation_update_document(params)
        validation = DocumentUpdateValidation.new
        validation.image = params[:image]
        validation.posX = params[:posX]
        validation.posY = params[:posY]
        validation
      end

    end
  end
end