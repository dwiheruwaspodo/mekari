class CreateDocuments < ActiveRecord::Migration[6.1]
  def change
    create_table :documents do |t|
      t.string :path
      t.string :token
      t.string :title
      t.timestamps
    end

    add_index :documents, :token, unique: true
  end
end
