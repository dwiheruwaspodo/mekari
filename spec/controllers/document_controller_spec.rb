require 'rails_helper'

RSpec.describe ::Api::V1::DocumentController, type: :controller do

  describe 'create' do
    it "routes create references" do
      should route(:post,"/api/v1/document").to(
        {:controller=>"document", :action=>"create"})
    end

    it 'successfully create a new document' do
      doc = ::Document.create({
                                token: SecureRandom::uuid,
                                path: "storage/whatever.pdf",
                                title: "document 1"
                              })

      expect(doc.last.title).to eq("document 1")
    end
  end

  describe 'update' do
    it "routes update references" do
      should route(:put,"/api/v1/document/123").to(
        {:controller=>"document", :action=>"update", :id=>123})
    end

    it 'successfully update with title' do
      doc = ::Document.create({
                                token: SecureRandom::uuid,
                                path: "storage/whatever.pdf",
                                title: "document 1"
                              })

      doc.title = "document 1 - update"
      doc.save

      expect(doc.last.title).to eq("document 1 - update")
    end
  end
end
