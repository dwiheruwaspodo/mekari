# README

Hallo, masyarakat. 

Ini aplikasi dimana kamu bisa upload pdf, kamu bisa menambahkan image dan text (berdasarkan title).
Untuk filenya disimpan di folder `/storage`.

Kalo mau nyobain, berikut collection postmannya : 

https://www.getpostman.com/collections/d3956dd70f41e67529be

Berikut yang harus diperhatikan yaaa ~

* Ruby version
    - 2.5.1
* System dependencies
    - Mysql
    - HexaPDF
    - Reactjs
    - Bootstrap
* Configuration
    - setup env (follow .env.example)
* Deployment instructions
    - run : `bundle install`
    - run : `yarn install`
    - run : `rails db:create`
    - run : `rails db:migrate`
    - run : `rails s`

